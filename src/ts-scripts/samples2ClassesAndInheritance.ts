class Vehicle {
  public move() {
    // tslint:disable-next-line:no-console
    console.info("This vehicle is moving...");
  }
}

// tslint:disable-next-line:max-classes-per-file
class Bike extends Vehicle {
  public burnCalories() {
    // tslint:disable-next-line:no-console
    console.info("I am a bike and I make my driver burning calories!");
  }
}

// tslint:disable-next-line:max-classes-per-file
class Car extends Vehicle {
  public produceSmog() {
    // tslint:disable-next-line:no-console
    console.info("I am a car and I produce awesome smog!");
  }
}

let vehicle = new Vehicle();
// [TIP: available methods]
// vehicle.

let myVehicle: Vehicle;
myVehicle = new Bike();
myVehicle.move();
// [TIP: I can also run burnCalories method cause it is a bike... ]
// myVehicle.burnCalories()

// ------------>>>> CASTING

// let myBike = myVehicle as Bike;
// if (myBike !== null) {
//   myBike.burnCalories();
// }
