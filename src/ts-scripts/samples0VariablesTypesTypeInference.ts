// Let's code some TypeScript !!
// tslint:disable-next-line:no-console
// console.info("AAAAAAAAAAAAA");

// --------------- VARIABLES ------------------------

let myUndefinedVariable;

let mySecondUndefinedVariable: undefined = undefined;

let myNullVariable: null = null;

let myAnyVariable: any = "Any"; // ( => the type given to this variable can be anything we like)
// QUESTION: Question: will it work if I uncomment the following?
// myAnyVariable = 3;

let someNumber: number; // ( => this is what we call an annotation = DECLARATION + TYPE)

let someText = "Hello and welcome to TypeScript"; // (=> TSC compiler is able to infer the type for you)
// [TIP: mouse over 'someText']

let someOtherNumber: number = 1;
// QUESTION: Can I assign another value type?
// someOtherNumber = " I won't be able to change this variable's type ..."

let someOtherText: string = "Watch at this!";
// QUESTION: will it work if I uncomment the following?
// someOtherText = 23;

let someBoolean: boolean = true;
// QUESTION: will it work if I uncomment the following?
// someBoolean = false;
// QUESTION: will it work if I uncomment the following?
// someBoolean = "false";

// -------------- ARRAYS --------------------------

let myFavoriteLanguages: string[] = [
  "JS",
  "C#",
  "JAVA"
  // 900
];

// QUESTION: what if I uncomment the last item of the array?

// QUESTION: what eventually could I do?

// ---------------- TYPE INFERENCE ---------------------------------

const num1 = 3;
const num2 = num1 + 1; // ( => QUESTION what type will num2 be?)

const someStr = num1 + "Some text"; // (QUESTION: what type someStr will be?)

// QUESTION:  is the following a legal statement?
// let testVariable : number = num1 + 'some text';

const sumFunction = function(n1: number, n2: number) {
  return n1 + num2;
};

// QUESTION: what type sum variable will be?
const sum = sumFunction(2, 5);
// tslint:disable-next-line:no-console
console.info("Sum of 2 and 5 is " + sum);
