// ---------------- GENERICS -------------------------

function identity<T>(value: T): T {
  return value;
}

const numericValue = identity(2);

class Numbers<T1, T2, T3> {
  constructor(
    private numericValue: T1,
    private englishName: T2,
    private italianName: T3
  ) {}

  // tslint:disable-next-line:member-ordering
  public toTuple(): [T1, T2, T3] {
    return [this.numericValue, this.englishName, this.italianName];
  }
}

let tuple1 = new Numbers(1, "one", "uno");
let tuple2 = new Numbers(2, "two", "due");
let tuple3 = new Numbers(3, "three", "tre");

let tuple500 = new Numbers("five-hundreds", 500, "cinquecento");

// ---------------- LAMBDAS -------------------------

function filterEntries<T>(items: T[], predicate: (item: T) => boolean) {
  const result: T[] = [];

  for (const item of items) {
    if (predicate(item)) {
      result.push(item);
    }
  }

  return result;
}

let evenNumbers = filterEntries(
  [1, 2, 3, 4, "intruder"],
  i => typeof i === "number" && i % 2 === 0
);

let shortNames = filterEntries(["Tom", "Jack", "Agamennon"], i => i.length < 9);

// ------------------- LINQ ----------------------------

function* whereOperator<T>(
  source: Iterable<T>,
  predicate: (item: T) => boolean
) {
  for (const item of source) {
    if (predicate(item)) {
      yield item;
    }
  }
}

let collection = [1, 3, 6, 7];
let lessThan5 = whereOperator(collection, x => x < 5);

// [TIP uncomment the next line]
// alert(lessThan5);

// [TIP uncomment the next line ]
// for (const item of lessThan5) {
//   alert(item);
// }

// ------------------- ITERATORS / GENERATORS ---------------

// YIELD: The function containing the yield keyword is a generator.
// A generator-iterator is returned.
// Each call to  its next() method performs another pass through the iterative algorithm.
// Each time you call next(), the generator code resumes from the statement following the yield.

// Think of yield as the generator-iterator version of return, indicating the boundary between each iteration of the algorithm.

function* return123() {
  for (let i = 1; i < 4; ++i) {
    yield i;
  }
}

let firstIterable = return123();
// tslint:disable-next-line:no-console
console.info(firstIterable);

// [TIP: iterable must be iterated to print values]
// for (const item of firstIterable) {
//   alert(item);
// }

function* return01234() {
  yield 0;
  yield* return123();
  yield 4;
}

let nestedIterable = return01234();
// [TIP: iterable must be iterated to print values]
// for (const item of iterable) {
//   alert(item);
// }
