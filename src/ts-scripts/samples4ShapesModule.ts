export namespace Shapes {
  interface IShape {
    calcArea: () => number;
  }

  export class Square implements IShape {
    constructor(public side: number) {}

    public calcArea() {
      return this.side * this.side;
    }
  }

  // tslint:disable-next-line:max-classes-per-file
  export class Circle implements IShape {
    constructor(public radius: number) {}

    public calcArea() {
      return this.radius * this.radius * Math.PI;
    }
  }

  // tslint:disable-next-line:max-classes-per-file
  export class Rectangle implements IShape {
    constructor(public longSize: number, public shortSize: number) {}

    public calcArea() {
      return this.longSize * this.shortSize;
    }
  }

  const mySquare = new Square(3);
  alert("The area of a square of size 3 is " + mySquare.calcArea());

  const myCircle = new Circle(3);
  alert("The area of a circle of radius 3 is " + myCircle.calcArea());
}

// Everything is defined within a module, Shapes!!
// If no namespace Shapes was defined, everything would extend the global namespace
// (the window object).

// TIP: I want to build an app which uses that module => EXPORT IT
