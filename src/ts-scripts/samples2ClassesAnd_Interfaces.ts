// -------------------- CLASSES AND INTERFACES -------------------

class Dog {
  private dogName: string;

  constructor(dogName: string) {
    this.dogName = dogName;
  }

  public speak() {
    // tslint:disable-next-line:no-console
    console.info("Bau!! My name is " + this.dogName);
  }

  public get DogName(): string {
    return this.dogName;
  }

  public set DogName(value: string) {
    if (value === undefined) {
      throw new Error("Supply a name for this poor dog!");
    }
    this.dogName = value;
  }
}

let myDog = new Dog("Bobby");
myDog.speak();

// [TIP: change dog name by property]
// myDog.DogName = 'Pongo';
// myDog.speak();

// ----------------------------------->>>>>>>>>>>>>>>>>>>>>>

// [TIP: now define a class Family, which comes with a Dog)

// tslint:disable-next-line:max-classes-per-file
class Family {
  private surname: string;
  private dog: Dog; // [TIP => => can be something else...]
  //

  constructor(surname: string, dog: Dog) {
    this.surname = surname;
    this.dog = dog;
  }

  get Dog(): Dog {
    return this.dog;
  }

  public greet() {
    this.dog.speak();
    alert("Hi, we are the " + this.surname + "s!");
  }
}

// [SAMPLE OF USAGE]
let theSimpson = new Family("Simpson", new Dog("Santa's Little Helper"));
theSimpson.greet();
// [TIP: the Simpsons have as well a cat ...]

// ---------------->>

interface IAnimal {
  animalName: string;
  happiness: number;

  speak(): void;
  receiveFood(): number;
}

// (Interfaces provide a contract, which other the objects I expect MUST implement!)

// tslint:disable-next-line:max-classes-per-file
class Cat implements IAnimal {
  public animalName: string;
  public happiness: number = 0;

  constructor(catName: string) {
    this.animalName = catName;
  }

  public speak() {
    // tslint:disable-next-line:no-console
    console.info("Miao!");
  }

  public receiveFood() {
    this.happiness++;
    return this.happiness;
  }
}

// tslint:disable-next-line:max-classes-per-file
class ProperDog implements IAnimal {
  public happiness: number = 5;
  public animalName: string;

  constructor(catName: string) {
    this.animalName = catName;
  }

  public speak() {
    // tslint:disable-next-line:no-console
    console.info("Bau!");
  }

  public receiveFood() {
    this.happiness += 2;
    return this.happiness;
  }
}

// tslint:disable-next-line:max-classes-per-file
class Family2 {
  private surname: string;
  private pet: IAnimal;

  constructor(surname: string, pet: IAnimal) {
    this.surname = surname;
    this.pet = pet;
  }

  get Pet(): IAnimal {
    return this.pet;
  }

  public greet() {
    this.pet.speak();
    alert("Hi, we are the " + this.surname + "s!");
  }
}

// [TIP: SAMPLE]
let properFamily = new Family2("Simpson", new Cat("Snowball"));
properFamily.greet();
