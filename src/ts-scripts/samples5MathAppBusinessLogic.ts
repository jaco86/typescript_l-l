import { Shapes } from "./samples4ShapesModule";
export namespace BusinessLogic {
  export class MathLogic {
    public getArea(shapeName: string, size: number[]) {
      // let shape: any;
      // [TIP: Uncomment the factory and the following line):
      const shape = ShapeFactory.createShape(shapeName, size);

      if (shape === undefined) {
        return;
      }
      return shape.calcArea();
    }
  }

  // tslint:disable-next-line:max-classes-per-file
  class ShapeFactory {
    public static createShape(shapeName: string, size: number[]) {
      if (shapeName === "square") {
        return new Shapes.Square(size[0]);
      } else if (shapeName === "circle") {
        return new Shapes.Circle(size[0]);
      } else if (shapeName === "rectangle") {
        return new Shapes.Rectangle(size[0], size[1]);
      }

      return undefined;
    }
  }
}
