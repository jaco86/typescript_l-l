// TIP: uncomment the following import
// import { BusinessLogic } from "./samples5MathAppBusinessLogic";

// TIP: [remove previous module alerts]

namespace ModularApp {
  // This app simulates a shape's area calculator
  export class App {
    public run() {
      // const businessLogicModule = new BusinessLogic.MathLogic();
      // (Wait for user's input/shape choice)
      // const userValue = 5;
      // const squareArea = businessLogicModule.getArea("square", [userValue]);
      // alert("The area of a square of side " + userValue + " is " + squareArea);
      // const circleArea = businessLogicModule.getArea("circle", [userValue]);
      // alert(
      //   "The area of a circle of radius " + userValue + " is " + circleArea
      // );
      // const rectArea = businessLogicModule.getArea("rectangle", [3, 4]);
      // alert("The area of a rectangle of sizes 3 and 4 is " + rectArea);
    }
  }
}

import AreaCalculator = ModularApp;

const appInstance = new AreaCalculator.App();
appInstance.run();

// TIP1: to import a specific portion of the module, we can use require
// tslint:disable-next-line:no-var-requires
// const {Square} = require('./Shapes');

// TIP2
// const a = new Shapes.Square(6);
// a.calcArea();
