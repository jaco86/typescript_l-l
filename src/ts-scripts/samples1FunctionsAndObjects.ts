// ------------------ FUNCTIONS -------------------------

let employeeName: string = "Fabrizio";
let employeeSurname: string = "Iacovone";

let buildFullNameAny = function(name: any, surname: any): any {
  return name + surname;
};

let buildFullNameAnyArrow = (name: any, surname: any) => {
  return name + surname;
};

// QUESTION: can I invoke the function with numbers?
// const withNumbers = buildFullNameAny(5, 4);
// alert('Result of invoking buildFullNameAny(5,4) is ' + fullName);

// QUESTION: what if I add annotations in the function buildFullNameProper (string, string)?
// TIP Uncomment the following

const buildFullNameProper = (firstName: string, lastName: string): string => {
  return `${firstName} ${lastName}`;
};
// alert('Result of invoking buildFullNameProper(name, surname) is ' + fullName);
// buildFullNameProper(3, 4);

// QUESTION: what if I invoke buildFullNameProper with a different number of parameter?
// buildFullNameProper('Vennegor', 'of', 'Hesselink');

// TIP: Or.. we can modify the signature of the function to make the parameter optional!
// buildFullNameProper('Jan');

// -------------- OBJECTS -------------------------------------------

const circle = {
  x: 0,
  y: 0,
  // tslint:disable-next-line:object-literal-sort-keys
  calcArea(radius: number): number {
    return radius * radius * Math.PI;
  }
};

const circleWithArrow = {
  x: 0,
  y: 0,
  // tslint:disable-next-line:object-literal-sort-keys
  calcAreaArrow: (r: number): number => r * r * Math.PI
};

const areaValue = circle.calcArea(5);
alert("Area of circle centered in (0,0) and with ray 5 is " + areaValue);

const areaValue2 = circleWithArrow.calcAreaArrow(5);
alert("Area of circle centered in (0,0) and with ray 5 is " + areaValue2);
