import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";

import "./ts-scripts/samples0VariablesTypesTypeInference";
// import './ts-scripts/samples1FunctionsAndObjects';
// import './ts-scripts/samples2ClassesAnd_Interfaces';
// import './ts-scripts/samples2ClassesAndInheritance';
// import "./ts-scripts/samples3GenericAndLambdas";
// import "./ts-scripts/samples4ShapesModule";
// import "./ts-scripts/samples5MathAppBusinessLogic";
// import "./ts-scripts/samples6ModularApp";

ReactDOM.render(<App />, document.getElementById("root") as HTMLElement);
registerServiceWorker();
