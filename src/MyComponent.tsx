// import _ from "lodash";
import * as React from "react";

interface IProps {
  personName: string;
  age?: number;
}

interface IState {
  greet: string;
}

export class MyComponent extends React.Component<IProps, IState> {
  constructor(props: any) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleGreetChange = this.handleGreetChange.bind(this);
  }

  state: IState = {
    greet: "Good afternoon"
  };

  handleClick = (e: React.FormEvent<HTMLButtonElement>) => {
    // tslint:disable-next-line:no-console
    console.info(e);

    alert(`${this.state.greet} ${this.props.personName}`);
  };

  handleGreetChange = (e: React.FormEvent<EventTarget>) => {
    const {
      name,
      value
    }: { name: string; value: string } = e.target as HTMLInputElement;
    this.setState({
      [name as keyof IState]: value
    });
  };

  // tslint:disable-next-line:member-ordering
  public render() {
    const { personName } = this.props;

    return (
      <div>
        <div>
          With this app you can send your custom greetings to {personName}...
        </div>

        <br />

        <div>
          <input
            name="greet"
            onChange={this.handleGreetChange}
            value={this.state.greet}
          />
          <button
            style={{ height: "20px", width: "80px", marginLeft: "5px" }}
            onClick={this.handleClick}
          >
            GREET
          </button>
        </div>
      </div>
    );
  }
}

// TIP 2: My component will have props => Create an interface for the Props

// TIP3: how can we import libraries (f.i. lodash) =>
// we have 2 options: image.d.ts or run the following:
// yarn add -D @types/lodash
