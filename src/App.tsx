import * as React from "react"; // TIP: this is how we import React
import "./App.css";
import logo from "./logo.svg";
import { MyComponent } from "./MyComponent"; // TIP: The component we're gonna add

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.tsx</code> and save to reload.
        </p>
        <MyComponent personName="Fabrizio" age={25} />
      </div>
    );
  }
}

export default App;

// TIP: 1) how do we add Types to React -> Create a component.tsx
